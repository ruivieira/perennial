import Vue from "vue";

Vue.config.productionTip = false;
Vue.config.devtools = true;

import App from "@/App.vue";
import router from "@/router";
import store from "@/store";
import Vuex from "vuex";

Vue.use(Vuex);

new Vue({
  router,
  store,
  render: (h) => h(App),
}).$mount("#app");

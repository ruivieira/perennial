import { Project, Story } from "./Models";
import { PerennialDB } from "./LocalDB";

export async function getProjects(db: PerennialDB): Promise<Array<Project>> {
  const data = db.projects.toArray();
  return data;
}

export async function getProjectStories(id: number): Promise<Array<Story>> {
  const respose = await fetch(`/projects/id/${id}/stories/all/json`);
  return respose.json();
}

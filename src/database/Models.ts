export interface Story {
  id?: number;
  name?: string;
  description?: string;
  projectId?: number;
}

export interface Project {
  id?: number;
  name?: string;
  description?: string;
}

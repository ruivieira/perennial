import Dexie from "dexie";
import * as Models from "@/database/Models";

export class PerennialDB extends Dexie {
  projects: Dexie.Table<Models.Project, number>; // number = type of the primkey
  stories: Dexie.Table<Models.Story, number>; // number = type of the primkey

  constructor() {
    super("Perennial");

    this.version(1).stores({
      projects: "++id, name, description",
      stories: "++id,name,description,projectId",
    });
    this.projects = this.table("projects");
    this.stories = this.table("stories");
  }

  async init() {
    await this.projects.put({
      name: "Perennial",
      description: "The Perennial app",
    });
    await this.stories.put({
      name: "First issue",
      description: "My first issue",
      projectId: 1,
    });
  }
}

import Dexie from "dexie";
import { PerennialDB } from "@/database/LocalDB";

export function createDB(db: PerennialDB) {
  db.projects.put({
    name: "Perennial",
    description: "The Perennial app",
  });
}

import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);

import { PerennialDB } from "@/database/LocalDB";

const db = new PerennialDB();

import { Project, Story } from "@/database/Models";

interface State {
  projects: Array<Project>;
  stories: Array<Story>;
}

export default new Vuex.Store({
  state: {
    projects: Array<Project>(),
    stories: Array<Story>(),
  },
  mutations: {
    SAVE_PROJECTS(state: State, projects: Array<Project>) {
      state.projects = projects;
    },
    SAVE_STORIES(state: State, stories: Array<Story>) {
      state.stories = stories;
    },
    ADD_STORY(state: State, story: Story) {
      state.stories.push(story);
    },
  },
  actions: {
    loadProjects({ commit }) {
      console.log("Loading projects");
      db.projects.toArray().then((data: Array<Project>) => {
        console.log(data);
        commit("SAVE_PROJECTS", data);
      });
    },
    loadProjectStories({ commit }, { id }) {
      console.log(`Loading stories for project #${id}`);
      db.stories
        .where("projectId")
        .equals(Number(id))
        .toArray()
        .then((data: Array<Story>) => {
          console.log(data);
          commit("SAVE_STORIES", data);
        });
    },
    addStory({ commit }, { story }) {
      console.log(`Saving new story`);
      console.log(story);
      db.stories.put(story).then((key) => {
        // story.id = key;
        console.log(key);
        commit("ADD_STORY", story);
      });
    },
    initDB({ commit }) {
      console.log("Initialising DB");
      db.init();
    },
  },
  modules: {},
});
